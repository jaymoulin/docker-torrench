![logo](cross.gif)

Torrench (Torrent search CLI command line interface) - Docker Image 
===================================================================

[![Docker Pulls](https://img.shields.io/docker/pulls/jaymoulin/torrench.svg)](https://hub.docker.com/r/jaymoulin/torrench/)
[![Docker stars](https://img.shields.io/docker/stars/jaymoulin/torrench.svg)](https://hub.docker.com/r/jaymoulin/torrench/)
[![Bitcoin donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/btc.png "Bitcoin donation")](https://m.freewallet.org/id/374ad82e/btc)
[![Litecoin donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/ltc.png "Litecoin donation")](https://m.freewallet.org/id/374ad82e/ltc)
[![Watch Ads](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/utip.png "Watch Ads")](https://utip.io/femtopixel)
[![PayPal donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/ppl.png "PayPal donation")](https://www.paypal.me/jaymoulin)
[![Buy me a coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png "Buy me a coffee")](https://www.buymeacoffee.com/3Yu8ajd7W)
[![Become a Patron](https://badgen.net/badge/become/a%20patron/F96854 "Become a Patron")](https://patreon.com/femtopixel)

This image allows you to search and download torrent files (not a torrent client) in CLI (command line interface) easily thanks to Docker.
This image is based on [Kryptxy's Torrench](https://github.com/kryptxy/torrench)

Usage
---

```
docker run --rm -ti -v /path/to/your/downloads:/root/Downloads/torrench jaymoulin/torrench torrench "<search>" 
```

With `<search>` your search string and `/path/to/your/downloads` where you want torrent files to be downloaded

You can also remove the `torrench` command if you pass an argument beginning with a dash. For example:

```
docker run --rm -ti jaymoulin/torrench --help 
```

Use `--risky` as first parameter to enable search on "illegal" torrent sites like TPB... For example:
 
 ```
 docker run --rm -ti jaymoulin/torrench --risky "debian" 
 ```

Appendixes
---

### Install Docker

If you don't have Docker installed yet, you can do it easily in one line using this command
 
```
curl -sSL "https://gist.githubusercontent.com/jaymoulin/e749a189511cd965f45919f2f99e45f3/raw/054ba73080c49a0fcdbc6932e27887a31c7abce2/ARM%2520(Raspberry%2520PI)%2520Docker%2520Install" | sudo sh && sudo usermod -aG docker $USER
```
